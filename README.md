<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

## Description
This is a [Nest](https://github.com/nestjs/nest) API using TypeScript to present retrieving a birthday greeting by requesting Restful API.

Once the App (known as Targaryen) is up in , please visit [API doc](http://localhost:3000/doc/api/#/) to check API spec

## Installation

```bash
$ npm install

# Init DB
$ docker-compose start targaryen-db
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

