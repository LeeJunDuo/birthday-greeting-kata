process.env.TZ = 'UTC'

import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { VersioningType } from '@nestjs/common/enums/version-type.enum'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import * as config from 'config'
import { TargaryenLogger } from './util/Logger'

const appName = config.get('appName')
const port = config.get('port') || 3000
const { version } = require('../../package.json')
async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: new TargaryenLogger()
  })
  app.enableVersioning({
    type: VersioningType.URI
  })
  const swaggerConfig = new DocumentBuilder().setTitle(`${appName} Service`).setDescription(`API Doc of ${appName}`).setVersion(version).build()
  const document = SwaggerModule.createDocument(app, swaggerConfig)
  SwaggerModule.setup('/doc/api', app, document)
  await app.listen(port)
}
bootstrap().catch(async (error) => {
  console.error({ tag: 'main', msg: 'bootstrap error', error })
  process.exit(1)
})
