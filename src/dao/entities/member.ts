import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity({
  database: 'sys'
})
export class Member {
  @PrimaryGeneratedColumn()
  id: string

  @Column({
    name: 'first_name',
    nullable: false
  })
  firstName: string

  @Column({
    name: 'last_name',
    nullable: false
  })
  lastName: string

  @Column({
    nullable: false
  })
  gender: string

  @Column({
    nullable: false
  })
  birthday: string

  @Column({
    nullable: false
  })
  email: string
}
