import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Member } from './entities/member'
import { MemberDao } from './member.dao'

@Module({
  imports: [TypeOrmModule.forFeature([Member])],
  providers: [MemberDao],
  exports: [MemberDao]
})
export class DaoModule {}
