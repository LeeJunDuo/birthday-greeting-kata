import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Member } from './entities/member'
import { Repository } from 'typeorm'
import { TargaryenLogger } from '../util/Logger'

@Injectable()
export class MemberDao {
  private logger: TargaryenLogger
  constructor(
    @InjectRepository(Member)
    private readonly memberRepo: Repository<Member>
  ) {
    this.logger = new TargaryenLogger('MemberDao')
  }

  async getTotalMemberCount(): Promise<number> {
    try {
      return await this.memberRepo.createQueryBuilder('m').getCount()
    } catch (error) {
      this.logger.error(error)
      throw error
    }
  }
  async findAllMember(options: { shift: number }) {
    try {
      return await this.memberRepo.createQueryBuilder('m').getMany()
    } catch (error) {
      this.logger.error(error)
      throw error
    }
  }
}
