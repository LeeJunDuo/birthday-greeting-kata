import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { DaoModule } from './dao/dao.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { MemberModule } from './member/member.module'
import * as config from 'config'

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...config.get('db.targaryen-mysql'),
      entities: [`${__dirname}/dao/entities/*{.ts,.js}`]
    }),
    DaoModule,
    MemberModule
  ],
  controllers: [AppController]
})
export class AppModule {}
