import StackTrace from 'stacktrace-js'
import { ConsoleLogger } from '@nestjs/common'

export class TargaryenLogger extends ConsoleLogger {
  constructor(service?: string) {
    super(service)
  }
  /**
   * Write an 'error' level log.
   */
  error(...log: any[]) {
    this.errorAsync(log)
  }
  private async errorAsync(err: any, ...msg: any[]) {
    console.debug({ err })
    if (err instanceof Error) {
      const stackframes = await StackTrace.fromError(err)

      super.error({
        version: process.env.npm_package_version,
        tag: stackframes[0].getFunctionName(),
        error: err
      })
    } else {
      super.error(err)
    }
  }
}
