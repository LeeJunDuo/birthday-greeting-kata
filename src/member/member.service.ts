import { Injectable } from '@nestjs/common'
import * as R from 'ramda'
import { MemberDao } from '../dao/member.dao'
import { TargaryenLogger } from '../util/Logger'
import { ConvertType, MemberVO } from './definitions'
import { Converter } from './convert'

const moment = require('moment')

@Injectable()
export class MemberService {
  private logger: TargaryenLogger
  constructor(private memberDao: MemberDao) {
    this.logger = new TargaryenLogger('MemberService')
  }
  async getMemberByBirthday(now: Date) {
    try {
      const currentMoment = moment(now).utc('+0800')
      const month = currentMoment.month() + 1
      const date = currentMoment.date()

      const totalMemberCount = await this.memberDao.getTotalMemberCount()
      let result: MemberVO[] = []
      let shift = 0
      const BLOCK_SIZE = 1000
      do {
        const members = (await this.memberDao.findAllMember({ shift }))
          .filter((m) => {
            const birthday = moment(`${m.birthday} 00:00:00+0800`).utc()
            return birthday.month() + 1 === month && birthday.date() === date
          })
          .map((m) => R.pick(['firstName', 'lastName', 'email', 'gender', 'birthday'], m) as MemberVO)

        result = result.concat(...members)
        shift += BLOCK_SIZE
      } while (shift < totalMemberCount)

      return result
    } catch (error) {
      this.logger.error(error)
      throw error
    }
  }

  convertToGreeting(members: MemberVO[], type: ConvertType) {
    try {
      const converter = new Converter(type)
      return members.map(converter.convert)
    } catch (error) {
      this.logger.error(error)
      throw error
    }
  }
}
