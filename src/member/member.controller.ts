import { Controller, Get, Req, Res } from '@nestjs/common'
import { MemberService } from './member.service'
import { Request, Response } from 'express'
import { TargaryenLogger } from '../util/Logger'
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger'
import { BirthdayGreetingDTO, ConvertType, MemberVO } from './definitions'

const { toXML } = require('to-xml')

@ApiTags('user')
@Controller({
  version: '1',
  path: 'user'
})
export class MemberControllerV1 {
  private logger: TargaryenLogger
  constructor(private memberService: MemberService) {
    this.logger = new TargaryenLogger('MemberController')
  }

  @Get('/birthday-greeting')
  @ApiOperation({ summary: 'Simple Message' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiResponse({
    status: 200,
    description: 'Find members whose birthday is today',
    type: BirthdayGreetingDTO
  })
  async queryWinningInvoice(@Req() req: Request, @Res() res: Response) {
    try {
      const result = await this.memberService.getMemberByBirthday(new Date())
      return res.status(200).json({
        data: this.memberService.convertToGreeting(result, ConvertType.SimpleMessage)
      } as BirthdayGreetingDTO)
    } catch (error) {
      this.logger.error(error)
      return res.status(500).send()
    }
  }
}

@ApiTags('user')
@Controller({
  version: '2',
  path: 'user'
})
export class MemberControllerV2 {
  private logger: TargaryenLogger
  constructor(private memberService: MemberService) {
    this.logger = new TargaryenLogger('MemberController')
  }

  @Get('/birthday-greeting')
  @ApiOperation({ summary: 'Tailor-made Message for different gender' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiResponse({
    status: 200,
    description: 'Find members whose birthday is today',
    type: BirthdayGreetingDTO
  })
  async queryWinningInvoice(@Req() req: Request, @Res() res: Response) {
    try {
      const result = await this.memberService.getMemberByBirthday(new Date())
      return res.status(200).json({
        data: this.memberService.convertToGreeting(result, ConvertType.TailorMadeOfGender)
      } as BirthdayGreetingDTO)
    } catch (error) {
      this.logger.error(error)
      return res.status(500).send()
    }
  }
}

@ApiTags('user')
@Controller({
  version: '3',
  path: 'user'
})
export class MemberControllerV3 {
  private logger: TargaryenLogger
  constructor(private memberService: MemberService) {
    this.logger = new TargaryenLogger('MemberController')
  }

  @Get('/birthday-greeting')
  @ApiOperation({ summary: 'Message with an Elder Picture for those whose age is over 49' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiResponse({
    status: 200,
    description: 'Plus an picture link within returned data',
    type: BirthdayGreetingDTO
  })
  async queryWinningInvoice(@Req() req: Request, @Res() res: Response) {
    try {
      const result = await this.memberService.getMemberByBirthday(new Date())
      return res.status(200).json({
        data: this.memberService.convertToGreeting(result, ConvertType.ElderMember)
      } as BirthdayGreetingDTO)
    } catch (error) {
      this.logger.error(error)
      return res.status(500).send()
    }
  }
}

@ApiTags('user')
@Controller({
  version: '4',
  path: 'user'
})
export class MemberControllerV4 {
  private logger: TargaryenLogger
  constructor(private memberService: MemberService) {
    this.logger = new TargaryenLogger('MemberController')
  }

  @Get('/birthday-greeting')
  @ApiOperation({ summary: 'Simple Message with full name' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiResponse({
    status: 200,
    description: '',
    type: BirthdayGreetingDTO
  })
  async queryWinningInvoice(@Req() req: Request, @Res() res: Response) {
    try {
      const result = await this.memberService.getMemberByBirthday(new Date())
      return res.status(200).json({
        data: this.memberService.convertToGreeting(result, ConvertType.SimpleMessageWithFullName)
      } as BirthdayGreetingDTO)
    } catch (error) {
      this.logger.error(error)
      return res.status(500).send()
    }
  }
}

@ApiTags('user')
@Controller({
  version: '5',
  path: 'user'
})
export class MemberControllerV5 {
  private logger: TargaryenLogger
  constructor(private memberService: MemberService) {
    this.logger = new TargaryenLogger('MemberController')
  }

  @Get('/birthday-greeting')
  @ApiOperation({ summary: 'Simple Message but database changes' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiResponse({
    status: 200,
    description: 'Use MySQL instead of Postgres',
    type: BirthdayGreetingDTO
  })
  async queryWinningInvoice(@Req() req: Request, @Res() res: Response) {
    try {
      const result = await this.memberService.getMemberByBirthday(new Date())
      return res.status(200).json({
        data: this.memberService.convertToGreeting(result, ConvertType.SimpleMessage)
      } as BirthdayGreetingDTO)
    } catch (error) {
      this.logger.error(error)
      return res.status(500).send()
    }
  }
}

@ApiTags('user')
@Controller({
  version: '6',
  path: 'user'
})
export class MemberControllerV6 {
  private logger: TargaryenLogger
  constructor(private memberService: MemberService) {
    this.logger = new TargaryenLogger('MemberController')
  }

  @Get('/birthday-greeting')
  @ApiOperation({ summary: 'Simple Message but different output data format' })
  @ApiResponse({ status: 500, description: 'Internal Server Error' })
  @ApiResponse({
    status: 200,
    description: 'Response would be XML'
  })
  async queryWinningInvoice(@Req() req: Request, @Res() res: Response) {
    try {
      const result = await this.memberService.getMemberByBirthday(new Date())
      const data = this.memberService.convertToGreeting(result, ConvertType.SimpleMessage)
      return res.status(200).set('Content-Type', 'text/xml; charset=utf8').send(convertToXML(data))
    } catch (error) {
      this.logger.error(error)
      return res.status(500).send()
    }
  }
}

function convertToXML(data: MemberVO[]) {
  const obj = {
    '?xml version="1.0" encoding="utf8"?': null,
    members: data.map((m) => ({
      root: m
    }))
  }
  return toXML(obj, null, 2)
}
