export class BirthdayGreetingDTO {
  data: BirthdayGreeting[]
}
export interface BirthdayGreeting {
  title: string
  content: string
}

export interface BirthdayGreetingWithImage extends BirthdayGreeting {
  imageUri: string
}

export interface MemberVO {
  firstName: string
  lastName: string
  email: string
  gender: 'Male' | 'Female'
  birthday: string
  isElder?: boolean
}

export interface Convert {
  convert<T extends MemberVO>(member: T): any
}

export enum ConvertType {
  SimpleMessage,
  TailorMadeOfGender,
  ElderMember,
  SimpleMessageWithFullName
}
