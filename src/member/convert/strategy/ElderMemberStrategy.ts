import { ConvertStrategy } from './index'
import { MemberVO } from '../../definitions'

const moment = require('moment')

export class ElderMemberStrategy extends ConvertStrategy {
  readonly ELDER_AGE = 49
  private now: Date
  constructor(now: Date) {
    super()
    this.now = now
  }

  convert<T extends MemberVO>(member: T) {
    const isElder = moment(this.now).diff(member.birthday, 'year') > this.ELDER_AGE
    return {
      title: 'Subject: Happy birthday!',
      content: `Happy birthday, dear \`${member.firstName}\`!`,
      imageUri: isElder ? '{{mockImageUri}}' : undefined
    }
  }
}
