import { ConvertStrategy } from './index'
import { MemberVO } from '../../definitions'

let self
export class TailorMadeStrategy extends ConvertStrategy {
  private readonly TAILOR_MADE_DISCOUNT: { [key: string]: string } = {
    Male: '20%',
    Female: '50%'
  }
  private readonly TAILOR_MADE_ITEMS: { [key: string]: string[] } = {
    Male: ['White Wine', 'iPhone X'],
    Female: ['Cosmetic', 'LV Handbags']
  }
  constructor() {
    super()
    self = this
  }
  convert<T extends MemberVO>(member: T) {
    return {
      title: 'Subject: Happy birthday!',
      content:
        `Happy birthday, dear ${member.firstName}! \n` +
        `We offer special discount ${self.TAILOR_MADE_DISCOUNT[member.gender]} off for the following items: \n` +
        `${self.TAILOR_MADE_ITEMS[member.gender]}`
    }
  }
}
