import { Convert, MemberVO } from '../../definitions'

export class ConvertStrategy implements Convert {
  convert<T extends MemberVO>(member: T): any {
    throw new Error('Must Implement convert()')
  }
}
