import { ConvertStrategy } from './index'
import { MemberVO } from '../../definitions'

export class SimpleMessageStrategy extends ConvertStrategy {
  convert<T extends MemberVO>(member: T) {
    return {
      title: 'Subject: Happy birthday!',
      content: `Happy birthday, dear ${this.genDisplayName(member)}!`
    }
  }
  protected genDisplayName<T extends MemberVO>(member: T) {
    return member.firstName
  }
}

export class SimpleMessageWithFullNameStrategy extends SimpleMessageStrategy {
  protected genDisplayName<T extends MemberVO>(member: T) {
    return `${member.lastName}, ${member.firstName}`
  }
}
