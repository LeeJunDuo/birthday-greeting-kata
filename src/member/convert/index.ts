import { ConvertType, MemberVO } from '../definitions'
import { ConvertStrategy } from './strategy'
import { SimpleMessageStrategy, SimpleMessageWithFullNameStrategy } from './strategy/simpleMessageStrategy'
import { TailorMadeStrategy } from './strategy/tailorMadeStrategy'
import { ElderMemberStrategy } from './strategy/ElderMemberStrategy'
import * as R from 'ramda'

let self
export class Converter {
  private readonly strategy: ConvertStrategy
  constructor(type: ConvertType) {
    switch (type) {
      case ConvertType.SimpleMessage:
        this.strategy = new SimpleMessageStrategy()
        break
      case ConvertType.TailorMadeOfGender:
        this.strategy = new TailorMadeStrategy()
        break
      case ConvertType.ElderMember:
        this.strategy = new ElderMemberStrategy(new Date())
        break
      case ConvertType.SimpleMessageWithFullName:
        this.strategy = new SimpleMessageWithFullNameStrategy()
        break
      default:
        throw new Error('Invalid ConvertType')
    }
    self = this
  }
  convert(member: MemberVO) {
    if (R.isNil(self.strategy)) {
      throw new Error('Strategy is undefined')
    }
    return self.strategy.convert(member)
  }
}
