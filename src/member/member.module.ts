import { Module } from '@nestjs/common'
import { MemberControllerV1, MemberControllerV2, MemberControllerV3, MemberControllerV4, MemberControllerV5, MemberControllerV6 } from './member.controller'
import { MemberService } from './member.service'
import { DaoModule } from '../dao/dao.module'

@Module({
  imports: [DaoModule],
  controllers: [MemberControllerV1, MemberControllerV2, MemberControllerV3, MemberControllerV4, MemberControllerV5, MemberControllerV6],
  providers: [MemberService]
})
export class MemberModule {}
