import { Controller, Get } from '@nestjs/common'
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger'

const packageJson = require('../package.json')

@ApiTags('application')
@Controller()
export class AppController {
  constructor() {}

  @Get('/healthz')
  @ApiOperation({ summary: 'Check Targaryen is healthy or not' })
  @ApiResponse({
    status: 200,
    description: "It's healthy"
  })
  healthz(): object {
    return {
      ok: true
    }
  }

  @Get('/version')
  @ApiOperation({ summary: 'Retrieve service version' })
  @ApiResponse({
    status: 200,
    description: 'Retrieve version successfully'
  })
  version(): object {
    return {
      version: packageJson.version
    }
  }
}
