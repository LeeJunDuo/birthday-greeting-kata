FROM node:14-slim

ARG APP_HOME=/usr/src/app
WORKDIR ${APP_HOME}
COPY --chown=node:node . ${APP_HOME}

# Server Build
WORKDIR ${APP_HOME}
RUN npm run build

EXPOSE 3000

CMD ["npm", "run", "start:prod"]