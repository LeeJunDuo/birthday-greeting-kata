import { INestApplication } from '@nestjs/common'
import * as supertestRequest from 'supertest'
import { Test, TestingModule } from '@nestjs/testing'
import { AppModule } from '../src/app.module'
import { NestExpressApplication } from '@nestjs/platform-express'
import { VersioningType } from '@nestjs/common/enums/version-type.enum'
import { advanceTo as dateMockSetNow, clear as clearMockDate } from 'jest-date-mock'
import { MemberDao } from '../src/dao/member.dao'

describe('Member Controller', () => {
  let app: INestApplication
  let requester: supertestRequest.SuperTest<supertestRequest.Test>
  const memberDao = {
    getTotalMemberCount: jest.fn(),
    findAllMember: jest.fn()
  }

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule]
    })
      .overrideProvider(MemberDao)
      .useValue(memberDao)
      .compile()

    app = moduleRef.createNestApplication<NestExpressApplication>()
    app.enableVersioning({
      type: VersioningType.URI
    })

    await app.listen(8800)
    await app.init()
    requester = supertestRequest(app.getHttpServer())
  })
  afterEach(() => {
    clearMockDate()
  })
  afterAll(async () => {
    await app.close()
  })

  beforeEach(async () => {
    jest.clearAllMocks()

    memberDao.getTotalMemberCount.mockResolvedValue(1)
    memberDao.findAllMember.mockResolvedValue([
      {
        firstName: 'David',
        lastName: 'Wang',
        gender: 'Male',
        birthday: '1960-10-23',
        email: 'david.wang@gmail.com'
      }
    ])
  })

  describe("GET users' birthday greeting", () => {
    const genGet = (version: string) => {
      const url = `/${version}/user/birthday-greeting`
      return (data?: any) => {
        return requester.get(url).set('Content-Type', 'application/json').query(data)
      }
    }
    describe('v1', () => {
      const get = genGet('v1')
      it('retrieve members whose birthday is today successfully', async () => {
        dateMockSetNow('2022-10-23 00:00:00+0800')
        await get()
          .expect(200)
          .expect({
            data: [
              {
                title: 'Subject: Happy birthday!',
                content: 'Happy birthday, dear David!'
              }
            ]
          })
      })
    })
    describe('v2', () => {
      const get = genGet('v2')
      it('retrieve members whose birthday is today successfully', async () => {
        dateMockSetNow('2022-10-23 00:00:00+0800')

        resetDB([
          {
            firstName: 'David',
            lastName: 'Wang',
            gender: 'Male',
            birthday: '1960-10-23',
            email: 'david.wang@gmail.com'
          },
          {
            firstName: 'Lucy',
            lastName: 'Chang',
            gender: 'Female',
            birthday: '1980-10-23',
            email: 'lucy.chang@gmail.com'
          }
        ])

        await get()
          .expect(200)
          .expect({
            data: [
              {
                title: 'Subject: Happy birthday!',
                content: 'Happy birthday, dear David! \n' + 'We offer special discount 20% off for the following items: \n' + 'White Wine,iPhone X'
              },
              {
                title: 'Subject: Happy birthday!',
                content: 'Happy birthday, dear Lucy! \n' + 'We offer special discount 50% off for the following items: \n' + 'Cosmetic,LV Handbags'
              }
            ]
          })
      })
    })
    describe('v3', () => {
      const get = genGet('v3')
      it('Add imageUri in return whom is over 49', async () => {
        dateMockSetNow('2022-10-23 00:00:00+0800')

        resetDB([
          {
            firstName: 'David',
            lastName: 'Wang',
            gender: 'Male',
            birthday: '1960-10-23',
            email: 'david.wang@gmail.com'
          },
          {
            firstName: 'Lucy',
            lastName: 'Chang',
            gender: 'Female',
            birthday: '1980-10-23',
            email: 'lucy.chang@gmail.com'
          }
        ])

        await get()
          .expect(200)
          .expect({
            data: [
              {
                title: 'Subject: Happy birthday!',
                content: 'Happy birthday, dear `David`!',
                imageUri: '{{mockImageUri}}'
              },
              {
                title: 'Subject: Happy birthday!',
                content: 'Happy birthday, dear `Lucy`!'
              }
            ]
          })
      })
    })
    describe('v4', () => {
      const get = genGet('v4')
      it('retrieve members whose birthday is today successfully', async () => {
        dateMockSetNow('2022-10-23 00:00:00+0800')
        await get()
          .expect(200)
          .expect({
            data: [
              {
                title: 'Subject: Happy birthday!',
                content: 'Happy birthday, dear Wang, David!'
              }
            ]
          })
      })
    })
    describe('v5', () => {
      const get = genGet('v5')
      it('retrieve members whose birthday is today successfully', async () => {
        dateMockSetNow('2022-10-23 00:00:00+0800')
        await get()
          .expect(200)
          .expect({
            data: [
              {
                title: 'Subject: Happy birthday!',
                content: 'Happy birthday, dear David!'
              }
            ]
          })
      })
    })
    describe('v6', () => {
      const get = genGet('v6')
      it('retrieve members whose birthday is today successfully', async () => {
        dateMockSetNow('2022-10-23 00:00:00+0800')
        await get()
          .expect(200)
          .expect(
            '<?xml version="1.0" encoding="utf8"?>\n' +
              '<members>\n' +
              '  <root>\n' +
              '    <title>Subject: Happy birthday!</title>\n' +
              '    <content>Happy birthday, dear David!</content>\n' +
              '  </root>\n' +
              '</members>'
          )
      })
    })

    function resetDB(mockMember: any[]) {
      memberDao.getTotalMemberCount.mockReset()
      memberDao.findAllMember.mockReset()

      memberDao.getTotalMemberCount.mockResolvedValue(mockMember.length)
      memberDao.findAllMember.mockResolvedValue(mockMember)
    }
  })
})
