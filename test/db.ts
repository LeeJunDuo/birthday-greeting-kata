import { Connection, DeepPartial, getConnection as _getConnection, ObjectType } from 'typeorm'
import { Member } from '../src/dao/entities/member'

export const clean = async () => {
  const promises: Promise<any>[] = []
  const targaryenConnection = getConnection()
  if (targaryenConnection.isConnected) {
    // truncate tables
    const tables = targaryenConnection.entityMetadatas.map((metadata) => `${metadata.tableName}`)
    promises.push(targaryenConnection.query('USE sys;'))
    promises.push(targaryenConnection.query(`TRUNCATE TABLE ${tables.join(',')} ;`))
  }

  await Promise.all(promises)
}

export function getRepository<Entity>(entity: ObjectType<Entity>) {
  return _getConnection().getRepository(entity)
}
function getConnection(connectionName?: string) {
  return _getConnection() //  connectionName is "default"
}

export const createMember = async (arg: { firstName: string; lastName: string; gender: 'Male' | 'Female'; birthday: string; email: string }) => {
  const client = _getConnection()
  const members = (await tableDataAdd(client, Member, arg)) as Member | Member[]
  return { members }
}
const tableDataAdd = async <Entity>(client: Connection, entity: ObjectType<Entity>, data: DeepPartial<Entity>) => {
  const repo = await client.getRepository(entity)
  if (Array.isArray(data)) {
    return repo.save(data)
  } else {
    return repo.save([data])
  }
}
