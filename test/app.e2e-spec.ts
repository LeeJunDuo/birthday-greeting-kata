import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication } from '@nestjs/common'
import * as supertestRequest from 'supertest'
import { AppController } from '../src/app.controller'

const packageJson = require('../package.json')

describe('AppController (e2e)', () => {
  let app: INestApplication
  let requester: supertestRequest.SuperTest<supertestRequest.Test>

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      controllers: [AppController]
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()

    requester = supertestRequest(app.getHttpServer())
  })

  afterAll(async () => {
    await app.close()
  })

  it('Get version', async () => {
    await requester.get('/version').expect(200).expect({ version: packageJson.version })
  })

  it('Health Check', async () => {
    await requester.get('/healthz').expect(200).expect({ ok: true })
  })
})
